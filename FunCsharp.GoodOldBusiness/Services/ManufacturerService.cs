﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FunCsharp.GoodOldBusiness.Interfaces;
using FunCsharp.GoodOldBusiness.Models;

namespace FunCsharp.GoodOldBusiness.Services
{
    public class ManufacturerService : IManufacturerService
    {
        private IManufacturerReader manufacturerReader;
        private IManufacturerWriter manufacturerWriter;

        public ManufacturerService(
            IManufacturerReader manufacturerReader,
            IManufacturerWriter manufacturerWriter)
        {
            this.manufacturerReader = manufacturerReader;
            this.manufacturerWriter = manufacturerWriter;
        }

        public async Task<Manufacturer> CreateManufacturer(Manufacturer manufacturer)
        {
            await manufacturerWriter.WriteManufacturer(manufacturer);
            return manufacturer;
        }

        public Task<IEnumerable<Manufacturer>> GetManufacturers() =>
            manufacturerReader.ReadManufacturers();
    }
}
