﻿using FunCsharp.GoodOldBusiness.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FunCsharp.GoodOldBusiness.Interfaces
{
    public interface IManufacturerService
    {
        Task<IEnumerable<Manufacturer>> GetManufacturers();

        Task<Manufacturer> CreateManufacturer(Manufacturer manufacturer);
    }
}
