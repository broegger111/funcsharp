﻿using FunCsharp.GoodOldBusiness.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FunCsharp.GoodOldBusiness.Interfaces
{
    public interface IManufacturerReader
    {
        Task<IEnumerable<Manufacturer>> ReadManufacturers();
    }
}
