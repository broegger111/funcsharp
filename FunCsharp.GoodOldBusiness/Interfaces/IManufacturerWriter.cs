﻿using FunCsharp.GoodOldBusiness.Models;
using System.Threading.Tasks;

namespace FunCsharp.GoodOldBusiness.Interfaces
{
    public interface IManufacturerWriter
    {
        Task WriteManufacturer(Manufacturer manufacturer); 
    }
}
