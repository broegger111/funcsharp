﻿using System;

namespace FunCsharp.GoodOldBusiness.Models
{
    public class Manufacturer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
