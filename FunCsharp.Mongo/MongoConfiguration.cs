﻿using FunCsharp.Mongo.PersistentModels;
using MongoDB.Driver;
using System.Security.Authentication;

namespace FunCsharp.Mongo
{
    public class MongoConfiguration
    {
        private IMongoDatabase database;

        public MongoConfiguration(string connectionString, string databaseName)
        {
            var settings = MongoClientSettings.FromUrl(new MongoUrl(connectionString));
            settings.SslSettings = new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            var mongoClient = new MongoClient(settings);
            database = mongoClient.GetDatabase(databaseName);
        }

        public IMongoCollection<PersistentManufacturer> GetPersistentManufacturerCollection()
        {
            var options = new CreateIndexOptions() { Unique = true };
            var field = new StringFieldDefinition<PersistentManufacturer>("Name");
            var indexDefinition = new IndexKeysDefinitionBuilder<PersistentManufacturer>().Ascending(field);
            var collection = database.GetCollection<PersistentManufacturer>("manufacturers");
            collection.Indexes.CreateOne(indexDefinition, options);

            return collection;
        }
    }
}
