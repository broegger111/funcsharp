﻿using FunCsharp.GoodOldBusiness.Interfaces;
using FunCsharp.GoodOldBusiness.Models;
using FunCsharp.Mongo.PersistentModels;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FunCsharp.Mongo
{
    public class ManufacturerReader : IManufacturerReader
    {
        private IMongoCollection<PersistentManufacturer> collection;

        public ManufacturerReader(IMongoCollection<PersistentManufacturer> collection)
        {
            this.collection = collection;
        }

        public async Task<IEnumerable<Manufacturer>> ReadManufacturers()
        {
            var persistentManufacturers = await collection.Find(_ => true).ToListAsync();
            return persistentManufacturers.Select(x => x.ToModel());
        }
    }
}
