﻿using FunCsharp.GoodOldBusiness.Models;
using System;

namespace FunCsharp.Mongo.PersistentModels
{
    public class PersistentManufacturer
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public static class PersistentManufacturerExtensions{
        public static PersistentManufacturer ToPersistent(this Manufacturer manufacturer) => 
            new PersistentManufacturer { Id = manufacturer.Id, Name = manufacturer.Name };

        public static Manufacturer ToModel(this PersistentManufacturer persistent) =>
            new Manufacturer { Id = persistent.Id, Name = persistent.Name };
    }
}
