﻿using System.Threading.Tasks;
using FunCsharp.GoodOldBusiness.Interfaces;
using FunCsharp.GoodOldBusiness.Models;
using FunCsharp.Mongo.PersistentModels;
using MongoDB.Driver;

namespace FunCsharp.Mongo
{
    public class ManufacturerWriter : IManufacturerWriter
    {
        private IMongoCollection<PersistentManufacturer> collection;

        public ManufacturerWriter(IMongoCollection<PersistentManufacturer> collection)
        {
            this.collection = collection;
        }

        public Task WriteManufacturer(Manufacturer manufacturer)
        {
            return collection.InsertOneAsync(manufacturer.ToPersistent());
        }
    }
}
