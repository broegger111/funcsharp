﻿using FunCsharp.GoodOldBusiness.Models;
using System;

namespace FunCsharp.Web.Dtos
{
    public class ManufacturerDto
    {
        public Guid ManufacturerId { get; set; }
        public string ManufacturerName { get; set; }
    }
    public static class ManufacturerDtoMapper
    {
        public static ManufacturerDto ToDto(this Manufacturer manufacturer) =>
            new ManufacturerDto { ManufacturerId = manufacturer.Id, ManufacturerName = manufacturer.Name };

        public static Manufacturer ToModel(this ManufacturerDto dto) =>
            new Manufacturer { Id = dto.ManufacturerId, Name = dto.ManufacturerName };
    }
}
