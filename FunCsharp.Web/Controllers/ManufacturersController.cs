﻿using FunCsharp.GoodOldBusiness.Interfaces;
using FunCsharp.Web.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FunCsharp.Web.Controllers
{
    public class ManufacturersController : Controller
    {
        private IManufacturerService manufacturerService;

        public ManufacturersController(IManufacturerService manufacturerService)
        {
            this.manufacturerService = manufacturerService;
        }

        [HttpGet]
        [Route("api/manufacturers")]
        public async Task<IEnumerable<ManufacturerDto>> Get()
        {
            return (await manufacturerService.GetManufacturers()).Select(x => x.ToDto());
        }

        [HttpPost]
        [Route("api/manufacturers")]
        public async Task<IActionResult> Post([FromBody]ManufacturerDto dto)
        {
            var manufacturer = await manufacturerService.CreateManufacturer(dto.ToModel());
            return Created(Url.Link("comming soon", manufacturer.Id), manufacturer);
        }
    }
}
