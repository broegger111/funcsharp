﻿using FunCsharp.Mongo;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace FunCsharp.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetSection("MongoSettings")["ConnectionString"];
            var databaseName = Configuration.GetSection("MongoSettings")["DatabaseName"];
            var mongoConfiguration = new MongoConfiguration(connectionString, databaseName);

            services.AddMvc();
            services.Add(
                new ServiceDescriptor(
                    typeof(IControllerActivator),
                    new ControllerActivator(mongoConfiguration)));
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
