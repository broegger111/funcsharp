﻿using FunCsharp.GoodOldBusiness.Services;
using FunCsharp.Mongo;
using FunCsharp.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;

namespace FunCsharp.Web
{
    public class ControllerActivator : IControllerActivator
    {
        private MongoConfiguration mongoConfiguration;

        public ControllerActivator(MongoConfiguration mongoConfiguration)
        {
            this.mongoConfiguration = mongoConfiguration;
        }
        public object Create(ControllerContext context)
        {
            var controllerType = context.ActionDescriptor.ControllerTypeInfo.AsType();

            if (controllerType == typeof(ManufacturersController))
            {
                var collection = mongoConfiguration.GetPersistentManufacturerCollection();
                var reader = new ManufacturerReader(collection);
                var writer = new ManufacturerWriter(collection);
                var service = new ManufacturerService(reader, writer);
                return new ManufacturersController(service);
            }
            return null;
        }

        public void Release(ControllerContext context, object controller)
        {
        }
    }
}
